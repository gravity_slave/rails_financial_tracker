$(document).ready(function () {


        drawChart();

});

function drawChart() {
    var w = 800;
    var h = 450;
    var margin = {
        top: 58,
        bottom: 100,
        left: 80,
        right: 40
    };

    var width = w - margin.left - margin.right;
    var height = h - margin.top - margin.bottom;

    //set color
    var color = d3.scale.category10();

    var JSONdata = $("#chart").data();




    // DATA ASSIGNATION ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var data_usd = JSONdata["usd"].map(function (old) {
        return {
            last_val: old.usd_val,
            created_at: old.created_at
        };

    });
    console.log(data_usd);


    var data_eur = JSONdata["usd"].map(function (old) {
        return {
            last_val: old.eur_val,
            created_at: old.created_at
        };
    });
    console.log(data_eur);

    var data_rur = JSONdata["usd"].map(function (old) {
        return {
            last_val: old.rur_val,
            created_at: old.created_at
        };
    });
    console.log(data_rur);


    // SVG AND CHART CREATION/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var svg = d3.select("#chart").append("svg")
        .attr("width", w)
        .attr("height", h);


    var chart = svg.append("g")
        .classed("display", true)
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // BTN DIV AND BUTTONS /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //div for btn group
    var btc_controls_group= d3.select("body")
                        .append("div")
                        .attr("id","btc-controls");
    //first btn - for btc/usd
    var btc_usd_btn = btc_controls_group.append("button")
        .html("btc/usd")
        .attr("id","usd");

    btc_usd_btn.on('click', function () {

            plot.call(chart,{
                data: data_usd,
                axis: {
                    x: xAxis,
                    y: yAxis
                }
            })



    });

    //second btn - for btc/eur
    var btc_eur_btn = btc_controls_group.append("button")
        .html("btc/eur")
        .attr("id","eur")
        .attr("state", 0);

    btc_eur_btn.on('click', function () {

            plot.call(chart,{
                data: data_eur,
                axis: {
                    x: xAxis,
                    y: yAxis
                }
            })



    });

    //third btn - for btc/rur
    var btc_rur_btn = btc_controls_group.append("button")
        .html("btc/rur")
        .attr("id","rur");


    btc_rur_btn.on('click', function () {


            plot.call(chart, {
                data: data_rur,
                axis: {
                    x: xAxis,
                    y: yAxis
                }
            })



    });

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    
    //time parsers//////////////////////////////////////////////////////////////////////////////////////////////////////

    var dateParser = d3.time.format("%Y-%m-%d %H:%M:%S").parse;

    var dateReplacer = function (something) {
        return something.replace(/T/, " ")
            .substr(0,19);
    };

    var parseDate = function (smthng) {
        return dateParser(dateReplacer(smthng));
    };

    ////////////////////////////////////////////////////////////


    // set the ranges  ---- INITIAL RANGES
    var x = d3.time.scale()
        .domain(d3.extent(data_usd, function(d){
            var date = parseDate(d.created_at);
            return date;
        }))
        .range([0,width]);

    var y = d3.scale.linear()
        .domain(d3.extent(data_usd, function (d) {
            return d.last_val;
        }))
        .range([height,0]);


    // set Axis
    var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom")
                .ticks(d3.time.minutes, 3 )
                .tickFormat(d3.time.format("%a %H:%M"));

    var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left")
                .ticks(5);


    // set the line
    var line = d3.svg.line()
        .x(function (d) {
            var date = parseDate(d.created_at);
            return x(date);
        })
        .y(function (d) {
            return y(d.last_val);
        });
    //set the area - this must be changable !!!!!!!!!!!!!!!
    var area = d3.svg.area()
        .x(function(d){
            var date = parseDate(d.created_at);
            return x(date);
        })
        .y0(height)
        .y1(function(d){
            return y(d.last_val);
        });


    function displayAxis(params) {
       $("g.x.axis").remove();
       $("g.y.axis").remove();

        this.append("g")
            .classed("x axis", true)
            .attr("transform", "translate(0," + height + ")")
            .call(params.axis.x);

        this.append("g")
            .classed("y axis", true)
            .attr("transform", "translate(0,0)")
            .call(params.axis.y);


    }

        function plot(params) {
            x.domain(d3.extent(params.data, function(d){
                var date = parseDate(d.created_at);
                return date;
            }));
            y.domain(d3.extent(params.data, function (d) {
                    return d.last_val;
                }));

            xAxis.scale(x);

            yAxis.scale(y);

             displayAxis.call(this,params);



            //enter()
            this.selectAll(".area")
                .data([params.data])
                    .enter()
                        .append("path")
                        .classed("area", true);

            this.selectAll(".trendline")
                .attr("d", function (d) {
                   return line(d);
                });

            this.selectAll(".point")
                .data(params.data)
                    .enter()
                        .append("circle")
                        .classed("point", true)
                        .attr("r",2);
            //update

                this.selectAll(".area")
                    .attr("d", function(d){
                        return area(d);
                    });
                this.selectAll(".point")
                    .attr("cx",function (d) {
                        var date = parseDate(d.created_at);
                        return x(date);
                    })
                    .attr("cy", function (d) {
                        // console.log(d);
                        // console.log(typeof d.usd_val === "number");

                        return y(d["last_val"])
                    });

            //exit
            this.selectAll(".trendline")
                .data([params.data])
                .exit()
                .remove();
            this.selectAll(".point")
                .data(params.data)
                .exit()
                .remove();
        }

        plot.call(chart,{
            data: data_usd,
            axis: {
                x: xAxis,
                y: yAxis
            }
        })


    // var replaced = dateReplacer(data[0].created_at);
    //  console.log(replaced);
    // console.log(dateParser(replaced));
    // console.log(dateParser(replaced).getHours());

}




