class BitkiWorker
  include Sidekiq::Worker

  def perform
    bitki_ar=["btc_usd","btc_eur","btc_rur"]
    btc_arr = []

    3.times do |i|
      btc_arr.push(JSON.parse(RestClient.get("https://btc-e.nz/api/3/ticker/#{bitki_ar[i]}")))
    end

    btc_response_of_last = btc_arr.map.with_index(0)  { |bitc,index| bitc[bitki_ar[index]]["last"] }

    BitocAll.create(usd_val: btc_response_of_last[0],
                              eur_val: btc_response_of_last[1],
                              rur_val: btc_response_of_last[2])



  end
end

Sidekiq::Cron::Job.create(name: 'Bitki worker - every 1min', cron: '*/1 * * * *', class: 'BitkiWorker')

