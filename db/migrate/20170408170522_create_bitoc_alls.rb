class CreateBitocAlls < ActiveRecord::Migration[5.0]
  def change
    create_table :bitoc_alls do |t|
      t.float :usd_val, precision: 3
      t.float :eur_val, precision: 3
      t.float :rur_val, precision: 3

      t.timestamps
    end
  end
end
